use std::net::SocketAddrV4;

fn main() {
    unsafe {
        let socket = match libc::socket(libc::AF_INET, libc::SOCK_STREAM, 0) {
            -1 => panic!("No fd"),
            n => n,
        };

        let addr = libc::sockaddr_in {
            sin_family: libc::AF_INET as _,
            sin_port: 8080,
            sin_addr: libc::in_addr {
                s_addr: libc::INADDR_ANY,
            },
            sin_zero: [0; 8],
        };
        if libc::bind(
            socket,
            &addr as *const _ as *const _,
            std::mem::size_of_val(&addr) as _,
        ) == -1
        {
            panic!("Error");
        }
        let mut storage: libc::sockaddr_storage = unsafe { std::mem::zeroed() };
        let mut len = std::mem::size_of_val(&storage) as libc::socklen_t;

        let res = unsafe { libc::getsockname(socket, &mut storage as *mut _ as *mut _, &mut len) };
        if res == -1 {
            panic!("Error");
        }
    }
}
